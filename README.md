Projet docker

Pré-requis : 
	docker-compose version 1.24.0
( $ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
)

Mise en place :
	-  systemctl mysql stop
	-  sudo systemctl stop openvpn
	-  verifier que le port 8100 est libre

Lancement
	- $ docker-compose up -d 
	- Ouvrir un navigateur sur le localhost:8100
